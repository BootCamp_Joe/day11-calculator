import './App.css';
import MultipleCounter from './components/MultipleCounter/MultipleCounter';

function App() {

  return (
    <div>
      <MultipleCounter />
    </div>
  );
}

export default App;
