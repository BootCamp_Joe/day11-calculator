import React from 'react'
import Counter from '../Counter/Counter';
import {useSelector} from "react-redux";
import {v4 as uuidv4} from 'uuid'

const CounterGroup = () => {
  const sizeFromStore = useSelector((state) => state.counter.size)

  return Array.from({length: sizeFromStore}).map((_, index) => {
    return (
        <Counter key={uuidv4()}/>
    )
  });
};

export default CounterGroup;