import CounterSizeGenerator from '../CounterSizeGenerator/CounterSizeGenerator';
import CounterGroupSum from '../CounterGroupSum/CounterGroupSum';
import CounterGroup from '..//CounterGroup/CounterGroup';
import { useSelector } from 'react-redux'
import './MultipleCounter.css'

const MultipleCounter = () => {
  const sizeFromStore = useSelector((state) => state.counter.size)

  return (
    <div className='multipleCounterContainer'>
      <CounterSizeGenerator/>
      <CounterGroupSum/>
      {
        sizeFromStore > 0 &&
        <CounterGroup/>
      }
    </div>
  )
}

export default MultipleCounter