import React, {useState } from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { updateSize, updateSum } from '../../app/counterSlice';
import './CounterSizeGenerator.css'

const CounterSizeGenerator = () => {
  const [isValidSize, setisValidSize] = useState(true)
  const [sizeInputString, setSizeInputString] = useState('')
  const sizeFromStore = useSelector((state) => state.counter.size)
  const dispatch = useDispatch();

  const isInteger = (text) => {
    return /^\d+$/.test(text);
  }

  const handleSizeChange = (event) => {
    if (isInteger(event.target.value)) {
      setisValidSize(true)
      dispatch(updateSize(parseInt(event.target.value)))
    }else {
      dispatch(updateSize(0))
      setisValidSize(false)
    }
    setSizeInputString(event.target.value)
    dispatch(updateSum(0))
  }

  return (
    <div className='counterSizeGeneratorContainer'>
      <label htmlFor='size'>size: </label>
      <input name='size' type='text' value={sizeFromStore? sizeFromStore: sizeInputString} onChange={handleSizeChange}/>
      {
        !isValidSize && <p className='errorMessage'>Invalid size!</p>
      }
    </div>
  )
}

export default CounterSizeGenerator