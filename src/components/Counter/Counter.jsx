import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { updateSum } from '../../app/counterSlice';
import './Counter.css'

const Counter = () => {
  const [number, setNumber] = useState(0)
  const sumFromStore = useSelector((state) => state.counter.sum)
  const dispatch = useDispatch();

  const increment = () => {
    setNumber((previousNumber) => previousNumber + 1)
    dispatch(updateSum(sumFromStore + 1))
  }

  const decrement = () => {
    setNumber((previousNumber) => previousNumber - 1)
    dispatch(updateSum(sumFromStore - 1))
  }
  
  return (
    <div className='counterContainer'>
      <button onClick={() => increment()}>+</button>
      <p>{number}</p>
      <button onClick={() => decrement()}>-</button>
    </div>
  )
}

export default Counter